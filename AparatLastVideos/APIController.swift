//
//  APIController.swift
//  AparatLastVideos
//
//  Created by amir on 11/13/16.
//  Copyright © 2016 amirs.eu. All rights reserved.
//

import UIKit
import Alamofire

class APIController: NSObject {
    
    
    let aparatFirstPageVideosUrl:NSURL = NSURL(string:"http://www.aparat.com/etc/api/lastvideos")!
    var nextPageUrl:NSURL!
    
    
    //MARK: - init and sharedInstance
    
    
    class var sharedInstance :APIController {
        struct Singleton {
            static let instance = APIController()
        }
        
        return Singleton.instance
    }
    
    
    override init() {
        super.init()
    }
    
    
    //MARK: - load last videos
    
    
    func loadAparatLastVideosFirstPage(onComplete:@escaping (_ success:Bool, _ data:[[String:Any]])->()){
    
        nextPageUrl = nil
        loadAparatLastVideosWithPageUrl(url: nil, onComplete: onComplete)
    }
    
    
    func loadAparatLastVideosWithPageUrl(url:NSURL!, onComplete:@escaping (_ success:Bool, _ data:[[String:Any]])->()) {
        
        var pageURL = url
        
        if pageURL == nil {
            pageURL = aparatFirstPageVideosUrl
        }
        
        
        Alamofire.request(self.aparatFirstPageVideosUrl.absoluteString!).responseJSON { response in
         
            
            if let JSON = response.result.value as? [String:Any] {
                
                //parse lastVideos
                if let lastVideos = JSON["lastvideos"] as? [[String:Any]]{
                    onComplete(true, lastVideos)
                    return
                }
                
                
                //set next page url
                if let pagingForwardUrl = JSON["pagingForwardUrl"] as? String{
                    self.nextPageUrl  = NSURL(string: pagingForwardUrl)
                }
                
            }
            
            
            onComplete(false, [])
            
        }
        
        
        
    }
    

}
