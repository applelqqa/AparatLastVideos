//
//  AparatVideoCollectionViewCell.swift
//  AparatLastVideos
//
//  Created by amir on 11/13/16.
//  Copyright © 2016 amirs.eu. All rights reserved.
//

import UIKit
import Kingfisher

class AparatVideoCollectionViewCell: UICollectionViewCell {
    
    
    var videoPoster:UIImageView!
    var videoTitle:UILabel!

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup(){

        videoPoster = UIImageView(frame: CGRect(x: self.frame.size.width-55, y: 0, width: 55, height: 55))
        videoPoster.autoresizingMask = [.flexibleLeftMargin]
        self.addSubview(videoPoster)
        videoTitle = UILabel(frame: CGRect(x: 0, y: 0, width: self.frame.size.width-55, height: 55))
        videoTitle.autoresizingMask = [.flexibleWidth]
        self.addSubview(videoTitle)
    }
    
    
    func setCellViewWith(imageUrl:String, title:String){
    
        let url = URL(string:imageUrl)
        videoPoster.kf.setImage(with: url)
        videoTitle.text = title
    }
    
}
