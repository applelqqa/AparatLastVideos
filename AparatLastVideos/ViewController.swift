//
//  ViewController.swift
//  AparatLastVideos
//
//  Created by amir on 11/13/16.
//  Copyright © 2016 amirs.eu. All rights reserved.
//

import UIKit
import Alamofire


class ViewController: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    
    var collectionView:UICollectionView!
    var navbar:UIView!
    var aparatLastVideos:[[String:Any]] = []
    var loadingIndicator:UIActivityIndicatorView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
        loadData()
    
    
    }
    


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - load data
    
    func loadData(){
     
        
        APIController.sharedInstance.loadAparatLastVideosFirstPage { (success:Bool, data:[[String : Any]]) in
            
            if success {
                self.aparatLastVideos = data
                self.collectionView.reloadData()
            }
            else{
                Tools.showAlertInViewController(viewController: self, alertMessage: "Error in Load data!")
            }
            
        }
    
    }

    
    //MARK: - init views
    func initViews(){
        
        self.view.backgroundColor = UIColor.white
        
        
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width:self.view.frame.size.width, height:55)
        collectionView = UICollectionView(frame: CGRect(x:0,y:0, width:self.view.frame.size.width, height:self.view.frame.size.height-64), collectionViewLayout: layout)
        
        self.view.addSubview(collectionView)
        collectionView.delegate   = self
        collectionView.dataSource = self
        
       // let collectionViewLayout = AparatLastViewsCollectionViewLayout()
    
        //collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height-64) )
        navbar = UIView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 64))
     
        
        navbar.autoresizingMask = [.flexibleHeight,.flexibleWidth,.flexibleBottomMargin]
        collectionView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
        
        
        collectionView.register(AparatVideoCollectionViewCell.classForCoder(), forCellWithReuseIdentifier: "AparatVideoCollectionViewCell")
        
        collectionView.backgroundColor = UIColor.white
        
        //basic views
        
        self.view.addSubview(collectionView)
        self.view.addSubview(navbar)
        
//        view.addConstraints([
//             NSLayoutConstraint(item: view, attribute: .width, relatedBy: .equal, toItem: collectionView, attribute: .width, multiplier: 1, constant: 0),
//             NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: collectionView, attribute: .width, multiplier: 1, constant: -64),
//             NSLayoutConstraint(item: view, attribute: .centerX, relatedBy: .equal, toItem: collectionView, attribute: .centerX, multiplier: 1, constant: 0),
//             NSLayoutConstraint(item: view, attribute: .centerY, relatedBy: .equal, toItem: collectionView, attribute: .centerY, multiplier: 1, constant: 0),
//            ])
        
//        view.addConstraints([
//            NSLayoutConstraint(item: view, attribute: .width, relatedBy: .equal, toItem: navbar, attribute: .width, multiplier: 1, constant: 0),
//            NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: navbar, attribute: .height, multiplier: 0, constant: 64),
//            NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem: navbar, attribute: .top, multiplier: 1, constant: 0),
//            NSLayoutConstraint(item: view, attribute: .left, relatedBy: .equal, toItem: navbar, attribute: .left, multiplier: 1, constant: 0)
//            ])
        
        navbar.layoutIfNeeded()
        view.layoutIfNeeded()
        
        navbar.backgroundColor = UIColor.gray
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
        
        //navbar views 
        
        loadingIndicator = UIActivityIndicatorView()
        let changeLayoutButton = UIButton(type: .system)
        
        changeLayoutButton.setTitle("Change Layout", for: .normal)
        
        

        loadingIndicator.autoresizingMask = [.flexibleBottomMargin,.flexibleTopMargin]
        
        
        navbar.addSubview(loadingIndicator)
        navbar.addSubview(changeLayoutButton)
        
        

        
        
        loadingIndicator.isHidden = true
        
        
        view.layoutIfNeeded()
        
    }
    
    
    
    
    //MARK: - collection view
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width, height: 55)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return aparatLastVideos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var cell:AparatVideoCollectionViewCell!
        
        if let acell = collectionView.dequeueReusableCell(withReuseIdentifier: "AparatVideoCollectionViewCell", for: indexPath) as? AparatVideoCollectionViewCell {
           cell = acell
        }
        
        
        let cellData = self.aparatLastVideos[indexPath.row]
        
        
        cell.setCellViewWith(imageUrl: cellData["small_poster"] as! String, title: cellData["title"] as! String)
        
        
        return cell
        
        
    }


}

